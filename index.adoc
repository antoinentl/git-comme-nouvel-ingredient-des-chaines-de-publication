= Git+++<br>+++ comme nouvel ingrédient+++<br>+++ des chaînes de publication+++<span class="curseur-clignotant">|</span>+++
:revealjs_theme: black

[abstract]
--
Antoine Fauchié > https://www.quaternum.net[www.quaternum.net] +
ÉCRIDIL > Montréal > 30 avril et 1er mai 2018
--

[NOTE.speaker]
--
Avant de débuter je me dois de prendre quelques instants pour remercier les organisateurs de ce colloque, et plus particulièrement René Audet, Servanne Monjour et Tom Lebrun, ainsi que les autres intervenants et les participants – j'ai moi-même hésité entre vous présenter cette communication ou participer à la session en parallèle.
--

== Préambule+++<span class="curseur-clignotant">|</span>+++
[NOTE.speaker]
--
Ce que je me dois également de préciser, c'est que cette communication s'inscrit dans une recherche plus globale sur les chaînes de publication – soit les méthodes et outils pour fabriquer des publications et plus particulièrement des livres.
Pour cela j'ai mis en place un processus de travail modulaire – comme travaillent de nombreux chercheurs : des articles, des interventions, des participations à des initiatives collectives et des projets de prototypage nourrissent un mémoire en cours de rédaction.
Pour comprendre l'ensemble de ce travail je vous invite à suivre la prochaine pré-publication de ce mémoire sur l'influence des technologies et des méthodes du développement web sur les chaînes de publication.
Et, surtout, j'ai l'occasion de travailler sur ces sujets avec des personnes merveilleuses, dont certaines sont présentes ici : Julie Blanc, Anthony Masure, Marcello Vitali-Rosati ou Benoît Epron, ou encore Thomas Parisot ou Joël Faucilhon.
--

== Conclusion anticipée
Git, un système de gestion de versions, formalise des pratiques encore très peu présentes dans l'édition, pourtant essentielles pour travailler avec des outils numériques.+++<span class="curseur-clignotant">|</span>+++

[NOTE.speaker]
--
Comme toute conclusion hâtive la mienne est imparfaite et vous conduira à vouloir en savoir plus sur les systèmes de gestion de versions – ce qui serait déjà un succès de mon point de vue. +
Dans le temps qu'il m'est offert je vais tenter de vous démontrer la justesse de cette conclusion anticipée, ou au moins évoquer ces "pratiques" peu présentes dans l'édition.
--

== Constats
Limites de la numérotation, écrasements successifs, étapes définitives, pas de visibilité globale, etc.+++<span class="curseur-clignotant">|</span>+++

[NOTE.speaker]
--
Dans l'édition la gestion des fichiers d'un projet éditorial se résume trop souvent à une numérotation, chaque nouvelle version venant *écraser* la précédente.
Les méthodes classiques de gestion de projet ont un point faible commun : les étapes sont *définitives* et difficilement réversibles. +
Il y a un manque de porosité entre ces étapes et les métiers qui correspondent à ces étapes.
--

== Les étapes d'une publication

[.step]
* créer un projet
* recevoir un texte
* signaler des modifications
* mettre en commun ces modifications
* générer les formats

[NOTE.speaker]
--
* un nouveau livre en préparation
* en provenance d'un auteur
* après une session de corrections
* par exemple entre l'éditeur, le correcteur et l'auteur : synchroniser, fusionner
* par exemple le PDF imprimeur ou le livre numérique au format EPUB

Cette liste est non exhaustive, mais cela permet d'avoir une vue d'ensemble.
--

== Versionner
Penser et travailler en versions ne nous aide pas seulement à comprendre comment un projet évolue au fil du temps, mais nous donne aussi notre mot à dire sur ce processus d'évolution.

David Demaree, _Git par la pratique_+++<span class="curseur-clignotant">|</span>+++

[NOTE.speaker]
--
Travailler en versions ? Le processus de travail n'est plus linéaire, mais itératif.
--

== Git
Utilisons des protocoles pour versionner du texte{nbsp}!

Les étapes de publication ont des équivalents dans un système de gestion de version très utilisé{nbsp}: Git.+++<span class="curseur-clignotant">|</span>+++

[NOTE.speaker]
--
Nous pouvons nous appuyer sur les méthodes et les outils utilisés dans le monde de l'informatique et plus particulièrement du développement web.
Les protocoles permettent de nous *accorder* pour travailler ensemble, avec les outils numériques.
Les deux pratiques d'écriture du code et du texte ont beaucoup de points communs.
Versionner du code c'est versionner du texte.
--

== Commandes

[.step]
* init
* add
* branch / commit
* fetch / diff / merge
* push

[NOTE.speaker]
--
Voici quelques exemples de commandes dans Git qui correspondent à des étapes de publication. Ces termes en anglais peuvent être facilement traduits :

* créer un projet
* recevoir un texte et l'intégrer au projet
* créer une branche de travail et enregistrer des modifications
* récupérer les modifications, afficher les différences entre des versions et fusionner ces modifications/versions
* pousser les modifications sur le dépôt commun et donc générer les formats avec des conditions. Git s'associe à des processus de déploiement continu, il est possible de définir des conditions pour déclencher des actions en fonction de certaines commandes.
--

== Utiliser Git
Git est un système conceptuel, des interfaces compréhensibles par le commun des mortels permettent son utilisation.+++<span class="curseur-clignotant">|</span>+++

[NOTE.speaker]
--
L'utilisation *classique* de Git est le terminal, outil qui n'est pas simple en premier abord.
Pour cette présentation j'ai volontairement utilisé une mise en forme proche de celle d'un terminal de commandes, c'est probablement une erreur.
En effet aujourd'hui Git peut être utilisé sans un terminal.
L'interface est presque un détail puisque des solutions existent ou existeront – il sera peut-être nécessaire d'en créer des spécifiques pour nos pratiques de publication.
--

== Parmi les systèmes
[L'homme] est _parmi_ les machines qui opèrent avec lui.

Gilbert Simondon, _Du mode d'existence des objets techniques_+++<span class="curseur-clignotant">|</span>+++

[NOTE.speaker]
--
Pour citer très rapidement Gilbert Simondon, philosophe de la technique bien connu, l'homme peut et doit s'associer aux machines, aux systèmes informatiques dans notre cas, et non plus être un opérateur ou un surveillant.
--

== Œuvrer avec les versions
Entretenir un rapport créatif aux machines n’est pas évident d’autant que ce dernier est parfois, voire souvent, empêché par les machines elles-mêmes et par la structure productive dans laquelle elles s’inscrivent.

Sophie Fétro, "Œuvrer avec les machines numériques", _Back Office_+++<span class="curseur-clignotant">|</span>+++

[NOTE.speaker]
--
Pour reprendre le titre de l'article de Sophie Fétro dans le numéro 1 de la revue Back Office, "Œuvrer avec les machines numériques", il s'agirait d'œuvrer avec les versions, de les
--

== Réintégrer les textes+++<span class="curseur-clignotant">|</span>+++

[NOTE.speaker]
--
Prenons encore un peu de recul. +
Le versionnement, et ici Git, est une opportunité fabuleuse pour réintégrer les textes et les livres que nous concevons, que nous fabriquons, et dont certains logiciels ou certaines méthodes nous ont éloignées. +
Je n'ai pas le temps de vous montrer comment certains éditeurs utilisent le versionnement, mais vous pouvez découvrir les livres numériques de Getty Publications, qui intègrent un fonctionnement basé sur Git.
--

== Merci
Antoine Fauchié +
https://www.quaternum.net[www.quaternum.net]

Support de présentation rédigé en https://asciidoctor.org/docs/what-is-asciidoc/[AsciiDoc], généré avec https://asciidoctor.org/[Asciidoctor] et https://github.com/asciidoctor/asciidoctor-reveal.js[asciidoctor-revealjs], https://gitlab.com/antoinentl/git-comme-nouvel-ingredient-des-chaines-de-publication/[dépôt en ligne ouvert], https://gitlab.com/antoinentl/git-comme-nouvel-ingredient-des-chaines-de-publication/pipelines[déploiement continu].

[NOTE.speaker]
--
Je vous remercie pour votre attention, et je suis disponible à la fin de cette session pour répondre à vos questions.
--
