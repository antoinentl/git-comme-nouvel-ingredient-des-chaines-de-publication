# Git comme nouvel ingrédient des chaînes de publication

## Introduction (4k)
Conclusion anticipée : Git, un système de gestion de versions, formalise des pratiques encore très peu présentes dans l'édition, pourtant essentielles pour travailler avec des outils numériques.

## 1. Constats (8k)
Constats à détailler : limites de la numérotation, écrasements successifs, étapes définitives, pas de visibilité globale, etc.

Les étapes de travail successives :

- créer un projet
- recevoir un texte
- signaler des modifications
- mettre en commun ces modifications
- générer les formats

## 2. Versionner (8k)
Du côté du monde du développement : origines et fonctionnement de Git.

Exemples de plusieurs chaînes de publication (cas du mémoire ?).

Penser et travailler en versions ne nous aide pas seulement à comprendre comment un projet évolue au fil du temps, mais nous donne aussi notre mot à dire sur ce processus d'évolution.

## 3. La dimension de système (8k)
Utiliser Git en pratique : différentes possibilités, des contraintes fortes, utiliser des interfaces intermédiaires, déplacer de le savoir-faire (du traitement de texte à des composants assemblés).

Nécessité d'utiliser des langages de balisage léger.

Ce que cela change : ne plus pensez _chaîne_ mais _système_.

## Conclusion (4k)
Opportunité.
Nécessité d'apprendre.
Déployer le livre.
