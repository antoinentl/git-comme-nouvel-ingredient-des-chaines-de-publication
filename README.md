# Git comme nouvel ingrédient des chaînes de publication

Dépôt des fichiers de la communication d'[Antoine Fauchié](https://www.quaternum.net/) pour le colloque [ÉCRIDIL](http://ecridil.ex-situ.info/) – Montréal, 30 avril et 1er mai 2018 : "Le livre, défi de design : l’intersection numérique de la création et de l’édition".


## Organisation du dépôt
Le fichier `proposition-de-communication.md` est la proposition _initiale_ pour le colloque.

Le support de présentation correspond au fichier `index.adoc`, il est rédigé au format [AsciiDoc](https://asciidoctor.org/docs/what-is-asciidoc/).

Les autres fichiers ne doivent pas être modifiés – hormis la feuille de style `reveal.js/css/theme/white.css`.

## Génération du support de présentation
Le fichier HTML `index.html` est généré grâce à [Asciidoctor](https://asciidoctor.org/) et [Asciidoctor-revealjs](https://github.com/asciidoctor/asciidoctor-reveal.js).

## Bibliographie
Une courte bibliographie est disponible sur Zotero : [https://www.zotero.org/antoinentl/items/collectionKey/82IWM5E9](https://www.zotero.org/antoinentl/items/collectionKey/82IWM5E9)
