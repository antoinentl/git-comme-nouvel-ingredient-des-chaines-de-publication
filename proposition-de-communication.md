# Soumission à l'appel de propositions d’interventions pour le colloque ÉCRiDiL
Le colloque ÉCRiDiL : "Le livre, défi de design : l’intersection numérique de la création et de l’édition". Informations :

## Titre
Git et la publication universitaire/académique  
Systèmes de gestion de version et chaînes de publication  
Publier avec Git  
Titre retenu : **Git comme nouvel ingrédient des chaînes de publication ?**

## Résumé court
Les systèmes de gestion de version sont largement employés pour écrire, éditer et publier du code informatique. Parmi ces systèmes Git est désormais le plus utilisé, popularisé par des plateformes comme GitHub, GitLab ou même GitBook. Git repose sur des commandes qui rappellent les différentes phases d'une chaîne de publication : copier (`git clone`), travailler sur une partie d'un projet (`git branch`), enregistrer puis proposer des modifications (`git commit` et `git push`), rassembler différentes contributions (`git merge`), etc. À partir de cet outil numérique, interrogeons les pratiques de publication : ont-elles besoin de l'influence du développement informatique en terme de conception, d'organisation et de production ? Les systèmes de révision et de validation des éditeurs doivent-ils s'inspirer d'un système de gestion de version ? Quels bénéfices les chaînes de publication tirent-elles de Git ? N'est-ce pas l'occasion de co-concevoir des livres d'une nouvelle façon ?

Cette proposition est d'ailleurs disponible sur un dépôt privé sur GitLab, accès ouvert sur demande.

## Notice biobibliographique brève
Chef de projet indépendant, formateur et enseignant, je suis spécialisé dans le domaine du livre numérique. Je suis intervenant extérieur dans le Master 2 Publication numérique de l'enssib (École nationale supérieure des sciences de l'information et des bibliothèques) depuis 2015, et je réalise par ailleurs actuellement un mémoire de Master 2 qui a comme sujet les chaînes de publication modulaires inspirées des technologies et méthodes du web. J'ai participé au colloque ÉCRiDiL 2016 avec une proposition qui s'intitulait "Le livre web comme objet d’édition ?", article qui figure dans l'ouvrage *Design et innovation dans la chaîne du livre*. Je publie une veille quotidienne et des articles de fond sur www.quaternum.net.
